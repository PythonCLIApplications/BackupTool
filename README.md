
# Backup Tool ([documentation](https://pythoncliapplications.gitlab.io/CLIApplicationsManager/includes/BackupTool/index.html))

A CLI utility to backup files on GNU/Linux.

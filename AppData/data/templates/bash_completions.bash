#!/bin/bash

# It would have been impossible to create this without the following post on Stack Exchange!!!
# https://unix.stackexchange.com/a/55622

type "{executable_name}" &> /dev/null &&
_decide_nospace_{current_date}(){
    if [[ ${1} == "--"*"=" ]] ; then
        type "compopt" &> /dev/null && compopt -o nospace
    fi
} &&
__backup_tool_cli_{current_date}(){
    local cur prev cmd job_dir job_files
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    job_dir="{full_path_to_app_folder}/UserData/backup_jobs"
    # List the files in a specific directory.
    job_files=("${job_dir}/"*.py)
    # Get the file names only.
    job_files=(${job_files[@]##*/})
    # Remove the extension names.
    job_files=(${job_files[@]%.*})

    case $prev in
        --jobs-rel|--globals-rel)
            COMPREPLY=( $( compgen -W "${job_files[*]}") )
            return 0
            ;;
        -j|-g)
            COMPREPLY=( $( compgen -W "${job_files[*]}" -- ${cur}) )
            return 0
            ;;
    esac

    # Handle --xxxxxx=
    if [[ ${prev} == "--"* && ${cur} == "=" ]] ; then
        compopt -o filenames
        COMPREPLY=(*)
        return 0
    fi

    # Handle --xxxxx=path
    if [[ ${prev} == "=" ]] ; then
        # Unescape space
        cur=${cur//\\ / }
        # Expand tilder to $HOME
        [[ ${cur} == "~/"* ]] && cur=${cur/\~/$HOME}

        if [[ ${cur} != *"/"* ]]; then
            COMPREPLY=( $( compgen -W "${job_files[*]}" -- ${cur}) )
            return 0
        fi

        # Show completion if path exist (and escape spaces)
        compopt -o filenames
        local files=("${cur}"*)
        [[ -e ${files[0]} ]] && COMPREPLY=( "${files[@]// /\ }" )
        return 0
    fi

    # Completion of commands.
    if [[ $COMP_CWORD == 1 ]]; then
        COMPREPLY=( $(compgen -W \
            "backup generate check_notification open_docs_webpage -h --help --manual --version" -- "${cur}") )
        return 0
    fi

    # Completion of options and sub-commands.
    cmd="${COMP_WORDS[1]}"

    case $cmd in
    "backup")
        COMPREPLY=( $(compgen -W "-j --jobs-rel= -J --jobs-full= -g --globals-rel= \
-G --globals-full=" -- "${cur}") )
        _decide_nospace_{current_date} ${COMPREPLY[0]}
        ;;
    esac

    if [[ $COMP_CWORD == 2 ]]; then
        case $cmd in
        "generate")
            COMPREPLY=( $(compgen -W 'jobs globals system_executable' -- "$cur" ) )
            return 0
            ;;
        "check_notification")
            COMPREPLY=( $(compgen -W "sound desktop mail" -- "$cur" ) )
            return 0
            ;;
        esac
    fi

    if [[ "${COMP_WORDS[2]}" == "mail" ]]; then
        COMPREPLY=( $(compgen -W "--globals-rel= --globals-full=" -- "$cur" ) )
        _decide_nospace_{current_date} ${COMPREPLY[0]}
        return 0
    fi
} &&
complete -F __backup_tool_cli_{current_date} {executable_name}
